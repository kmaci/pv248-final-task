import Queue
import urllib
import re
import threading


class TitlesThread(threading.Thread):
    def __init__(self, interface):
        threading.Thread.__init__(self)
        self.interface = interface
        self.q = Queue.Queue()
        self.alive = True

    def titles(self, arg):
        if arg == 'None':
            self.stop()
            self.interface.write('The thread will end. It will not be possible to use titles again.')
        else:
            self.q.put(arg)

    def run(self):
        while self.alive:
            if not self.q.empty():
                address = self.q.get()
                try:
                    f = urllib.urlopen(address)
                except Exception, e:
                    self.interface.write('error: ' + str(e))
                else:
                    content = f.read()
                    match = re.findall(r'<title>(.*)</title>', content)
                    title = "".join(match)
                    f.close()
                    self.interface.write(title)

    def stop(self):
        self.alive = False