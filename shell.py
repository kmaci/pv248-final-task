from interface import Interface
import commands.listdir
import commands.quit
import commands.titles


class Shell():
    def __init__(self, interface):
        self.interface = interface
        self.shelldir = {}

    def run(self):
        while True:
            line = self.interface.read()
            try:
                ret = self.process(line)
            except Exception, e:
                self.interface.write('error: ' + str(e))
            else:
                if ret:
                    self.interface.write(ret)

    def add_command(self, command_name, command_function):
        self.shelldir[command_name] = command_function

    def process(self, line):
        chunks = line.split()
        func = self.shelldir.get(chunks[0])
        param = ''.join(chunks[1:])
        if not func:
            return line
        else:
            return func(param)


if __name__ == '__main__':
    i = Interface()
    s = Shell(i)
    thr = commands.titles.TitlesThread(i)
    s.add_command('listdir', commands.listdir.listdir)
    s.add_command('quit', commands.quit.quit)
    s.add_command('titles', thr.titles)
    try:
        thr.start()
        s.run()
    except SystemExit:
        thr.stop()