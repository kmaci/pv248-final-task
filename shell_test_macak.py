import unittest
import shell
import interface
import commands.listdir


def return_number_plus_one(number):
    number = int(number)
    return number + 1


class TestShell(unittest.TestCase):
    def setUp(self):
        i = interface.Interface()
        self.shell = shell.Shell(i)

    def test_add_command_listdir(self):
        self.shell.add_command('listdir', commands.listdir)
        self.assertTrue('listdir' in self.shell.shelldir)

    def test_process_unknown_method(self):
        value = self.shell.process('some_unknown_method param')
        self.assertEquals('some_unknown_method param', value)

    def test_new_method(self):
        self.shell.add_command('return_number_plus_one', return_number_plus_one)
        value = self.shell.process('return_number_plus_one 5')
        self.assertEqual(6, value)

if __name__ == '__main__':
    unittest.main()